#!/bin/bash -ex
VERSION='1.6_r2'
BUILD_VARIANT='eng'
#~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b android-$VERSION
#~/bin/repo sync --quiet --current-branch -j4

nproc() {
    grep -w ^processor /proc/cpuinfo | wc -l
}

export USER=$(whoami)

# https://android.googlesource.com/platform/sdk/+/master/docs/howto_build_SDK.txt
export BUILD_NUMBER="${BUILD_VARIANT}.${VERSION}"
# https://source.android.com/setup/build/building#initialize
. build/envsetup.sh
# https://source.android.com/setup/build/building#choose-a-target
lunch sdk-${BUILD_VARIANT}
# https://source.android.com/setup/build/building#build-the-code
# dist: https://groups.google.com/forum/?fromgroups=#!topic/android-building/Y0xduZPWtUs
# sdk_repo: build/core/main.mk:236: *** The 'sdk' target may not be specified with any other targets.  Stop.
make -j$(nproc) sdk                    showcommands dist
# make: *** No rule to make target `win_sdk'.  Stop.
#make -j$(nproc) win_sdk                showcommands dist
# build/core/product_config.mk:173: *** No matches for product "sdk_arm64".  Stop:
#make -j$(nproc) PRODUCT-sdk_arm64-sdk  showcommands dist
#make -j$(nproc) PRODUCT-sdk_x86_64-sdk showcommands dist

#ls -lh \
#    out/host/linux-x86/sdk/android-sdk_${BUILD_NUMBER}_linux-x86.zip \
#    out/dist/android-sdk_${BUILD_NUMBER}_linux-x86.zip
ls -lh \
    out/host/linux-x86/sdk/android-sdk_${BUILD_VARIANT}.android_linux-x86.zip \
    out/dist/android-sdk_${BUILD_VARIANT}.android_linux-x86.zip
