#!/bin/bash -ex
~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b android-6.0.1_r81
~/bin/repo sync --quiet --current-branch -j4

# out/host/linux-x86/bin/jack-admin: line 27: USER: unbound variable
export USER=$(whoami)

. build/envsetup.sh
export BUILD_NUMBER='user.6.0.1_r81'

# https://source.android.com/setup/build/building#choose-a-target
lunch sdk-user
# Can't build sdk and win_sdk on the same 'make':
# build/core/main.mk:392: *** The 'sdk' target may not be specified with any other targets.  Stop.
# dist: https://groups.google.com/forum/?fromgroups=#!topic/android-building/Y0xduZPWtUs
make sdk -j$(nproc) showcommands dist
#make win_sdk -j$(nproc) showcommands dist

ls -lh \
    out/host/linux-x86/sdk/sdk/android-sdk_user.6.0.1_r81_linux-x86.zip \
    out/dist/android-sdk_user.6.0.1_r81_linux-x86.zip
