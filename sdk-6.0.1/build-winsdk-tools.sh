#!/bin/bash -ex
VERSION='6.0.1_r31'  # winsdk-tools fails with r81
BUILD_VARIANT='user'
~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b android-${VERSION}
~/bin/repo sync --quiet --current-branch -j4

# out/host/linux-x86/bin/jack-admin: line 27: USER: unbound variable
export USER=$(whoami)

# https://android.googlesource.com/platform/sdk/+/master/docs/howto_build_SDK.txt
export BUILD_NUMBER="${BUILD_VARIANT}.${VERSION}"

. build/envsetup.sh

# https://source.android.com/setup/build/building#choose-a-target
lunch sdk-${BUILD_VARIANT}
# winsdk-tools: https://android.googlesource.com/platform/sdk/+/master/docs/howto_build_SDK.txt
make winsdk-tools -j$(nproc) showcommands dist

# Notes:
# no .zip archives
# unnecessary object files
# platform-tools and build-tools are mixed in both bin/ and lib/
# no strip
#prebuilts/gcc/linux-x86/host/x86_64-w64-mingw32-4.8/bin/x86_64-w64-mingw32-strip out/host/windows-x86/lib/*
# => requires too much manual packaging

ls -lh \
    out/host/windows-x86/bin/
