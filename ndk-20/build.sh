#!/bin/bash -ex

# https://android.googlesource.com/platform/ndk/+/master/docs/Building.md

# Preparing build tree
# Make 'repo' accessible to the build system later
export PATH=~/bin:$PATH
repo init --quiet -u https://android.googlesource.com/platform/manifest -b ndk-r20b
repo sync --quiet --current-branch -j4

# Fix ø character support for prebuilts/ndk/platform/sysroot/NOTICE
(
cd ndk/ && patch -p1 <<EOF
diff --git a/ndk/checkbuild.py b/ndk/checkbuild.py
index 7e3e12ac..6566d914 100755
--- a/ndk/checkbuild.py
+++ b/ndk/checkbuild.py
@@ -2102,10 +2102,10 @@ def create_notice_file(path, for_group):
 
     licenses = set()
     for notice_path in notice_files:
-        with open(notice_path) as notice_file:
+        with open(notice_path, encoding='UTF-8') as notice_file:
             licenses.add(notice_file.read())
 
-    with open(path, 'w') as output_file:
+    with open(path, 'w', encoding='UTF-8') as output_file:
         # Sorting the contents here to try to make things deterministic.
         output_file.write(os.linesep.join(sorted(list(licenses))))
 
EOF
)

# Build NDK - proper build
(
    cd ndk/
    python checkbuild.py --no-build-tests
    python checkbuild.py --no-build-tests --system windows
    python checkbuild.py --no-build-tests --system windows64
)

ls -lh \
   out/dist/android-ndk-0-linux-x86_64.tar.bz2 \
   out/dist/android-ndk-0-windows-x86_64.zip \
   out/dist/android-ndk-0-windows.zip
