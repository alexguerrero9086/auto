#!/bin/bash -ex
VERSION='8.0.0_r36'
BUILD_VARIANT='user'
~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b android-$VERSION
~/bin/repo sync --quiet --current-branch -j4

export USER=$(whoami)

# https://android.googlesource.com/platform/sdk/+/master/docs/howto_build_SDK.txt
export BUILD_NUMBER="${BUILD_VARIANT}.${VERSION}"
# https://source.android.com/setup/build/building#initialize
. build/envsetup.sh
# https://source.android.com/setup/build/building#choose-a-target
lunch sdk-${BUILD_VARIANT}
# https://source.android.com/setup/build/building#build-the-code
# Can't build sdk and win_sdk on the same 'make':
# build/core/main.mk:392: *** The 'sdk' target may not be specified with any other targets.  Stop.
# dist: https://groups.google.com/forum/?fromgroups=#!topic/android-building/Y0xduZPWtUs
# sdk_repo: https://android.googlesource.com/platform/development/+/1c875445b6fc08333872bd295527fd1359b80e78
make -j$(nproc) sdk                    showcommands dist sdk_repo
make -j$(nproc) win_sdk                showcommands dist sdk_repo
make -j$(nproc) PRODUCT-sdk_arm64-sdk  showcommands dist sdk_repo
make -j$(nproc) PRODUCT-sdk_x86_64-sdk showcommands dist sdk_repo
#make -j$(nproc) PRODUCT-sdk_x86_64-userdebug sdk showcommands dist sdk_repo
# => build/core/main.mk:436: error: The 'sdk' target may not be specified with any other targets.

ls -lh \
    out/host/linux-x86/sdk/sdk/android-sdk_${BUILD_NUMBER}_linux-x86.zip \
    out/dist/android-sdk_${BUILD_NUMBER}_linux-x86.zip \
    out/host/windows/sdk/sdk/android-sdk_${BUILD_NUMBER}_windows.zip \
    out/dist/android-sdk_${BUILD_NUMBER}_windows.zip \
    out/dist/sdk-repo*.zip \
    out/dist/repo*.xml

# useful?
# out/target/product/generic/sdk-symbols-eng.android.zip
