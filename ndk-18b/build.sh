#!/bin/bash -ex

# https://android.googlesource.com/platform/ndk/+/master/README.md

# Preparing build tree
# Make 'repo' accessible to the build system later
export PATH=~/bin:$PATH
repo init --quiet -u https://android.googlesource.com/platform/manifest -b ndk-r18b
repo sync --quiet --current-branch -j4

# Build NDK - proper build
(
    cd ndk/
    python checkbuild.py --no-build-tests
)

ls -lh out/dist/android-ndk-0-linux-x86_64.tar.bz2
