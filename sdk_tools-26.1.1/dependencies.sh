#!/bin/bash -ex

# Configuration
echo 'APT::Install-Recommends "false";' > /etc/apt/apt.conf.d/00InstallRecommends

# clean-up non-free sources
# AFAICS we need to update the base image as of 2018-11-20 as it's already partially upgraded
cat <<EOF > /etc/apt/sources.list
deb http://archive.ubuntu.com/ubuntu/ trusty main universe
deb http://archive.ubuntu.com/ubuntu/ trusty-updates main universe
#deb http://security.ubuntu.com/ubuntu/ trusty-security main universe
#deb http://archive.ubuntu.com/ubuntu/ trusty-backports main universe
EOF
dpkg --add-architecture i386
apt-get update

echo 'dash dash/sh boolean false' | debconf-set-selections
DEBIAN_FRONTEND=noninteractive dpkg-reconfigure --pri=high dash



# https://source.android.com/setup/build/initializing
export DEBIAN_FRONTEND=noninteractive
# OpenJDK 8 N/A
#apt-get -y install openjdk-8-jdk
apt-get -y install wget
wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jre-headless_8u45-b14-1_amd64.deb
wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jre_8u45-b14-1_amd64.deb
wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jdk_8u45-b14-1_amd64.deb
checksumfile=$(mktemp)
cat <<'EOF' > $checksumfile
0f5aba8db39088283b51e00054813063173a4d8809f70033976f83e214ab56c0  openjdk-8-jre-headless_8u45-b14-1_amd64.deb
9ef76c4562d39432b69baf6c18f199707c5c56a5b4566847df908b7d74e15849  openjdk-8-jre_8u45-b14-1_amd64.deb
6e47215cf6205aa829e6a0a64985075bd29d1f428a4006a80c9db371c2fc3c4c  openjdk-8-jdk_8u45-b14-1_amd64.deb
EOF
sha256sum -c $checksumfile || exit 1
dpkg -i openjdk-8-jdk_8u45-b14-1_amd64.deb \
  openjdk-8-jre-headless_8u45-b14-1_amd64.deb \
  openjdk-8-jre_8u45-b14-1_amd64.deb || true
apt-get -y -f install


# Compiles 32-bit C++ programs
# 'emugen' compiled and run as 32-bit
apt-get -y install make binutils patch
apt-get -y install g++-multilib
apt-get -y install zip unzip libxml2-utils  # zip/unzip/zip + xmllint


# Repo
apt-get install -y curl ca-certificates git python
