#!/bin/bash -ex

# Configuration
echo 'APT::Install-Recommends "false";' > /etc/apt/apt.conf.d/00InstallRecommends

# clean-up non-free sources
# AFAICS we need to update the base image as of 2018-11-20 as it's already partially upgraded
cat <<EOF > /etc/apt/sources.list
deb http://archive.ubuntu.com/ubuntu/ trusty main universe
deb http://archive.ubuntu.com/ubuntu/ trusty-updates main universe
#deb http://security.ubuntu.com/ubuntu/ trusty-security main universe
#deb http://archive.ubuntu.com/ubuntu/ trusty-backports main universe
EOF
dpkg --add-architecture i386
apt-get update

echo 'dash dash/sh boolean false' | debconf-set-selections
DEBIAN_FRONTEND=noninteractive dpkg-reconfigure --pri=high dash


# https://source.android.com/setup/build/requirements
# The master branch of Android in AOSP comes with a prebuilt version
# of OpenJDK, so no additional installation is required.


# https://source.android.com/setup/build/initializing
apt-get -y install git-core gnupg flex bison gperf build-essential zip \
  curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 \
  lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev \
  libgl1-mesa-dev libxml2-utils xsltproc unzip

# Windows dependencies
# https://sites.google.com/a/android.com/tools/build  2015-07-23
apt-get -y install mingw32 tofrodos


# Repo
apt-get install -y curl ca-certificates git python
