#!/bin/bash -ex

# Configuration
echo 'APT::Install-Recommends "false";' > /etc/apt/apt.conf.d/00InstallRecommends

# clean-up non-free sources
sed -i -e 's/ restricted//' -e 's/ multiverse//' /etc/apt/sources.list
dpkg --add-architecture i386
apt-get update

echo 'dash dash/sh boolean false' | debconf-set-selections
DEBIAN_FRONTEND=noninteractive dpkg-reconfigure --pri=high dash



# http://source.android.com/source/initializing.html + fixes
export DEBIAN_FRONTEND=noninteractive
apt-get -y install openjdk-7-jdk
apt-get -y install bison git gperf libxml2-utils make python-networkx zip unzip
apt-get -y install g++-multilib zlib1g-dev:i386

# Windows dependencies
# https://sites.google.com/a/android.com/tools/build  2015-07-23
apt-get -y install mingw32 tofrodos



# Repo
apt-get install -y curl ca-certificates git python
