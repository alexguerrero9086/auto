#!/bin/bash -ex

## Preparing build tree
git clone https://android.googlesource.com/platform/ndk.git ndk
git clone https://android.googlesource.com/platform/development.git development
git clone https://android.googlesource.com/platform/bionic.git bionic

(cd ndk/         && git checkout 0de21a10c0a96c3570a283462dc213fcac373607)
(cd development/ && git checkout $(git rev-list -n 1 --before='2015-04-07T11:32:42+0000' HEAD))
(cd bionic/      && git checkout $(git rev-list -n 1 --before='2015-04-07T11:32:42+0000' HEAD))


## Build NDK - grab all sources
export USER=$(whoami)
export NDK=~/wd/ndk
export NDK_LOGFILE=~/wd/ndk.log
bash $NDK/build/tools/download-toolchain-sources.sh --git-date='2015-04-07T11:32:42+0000' ~/wd/ndk-dl

## Build NDK - fix-ups
sed -i -e 's/LIBS="@LIBS@ $SYSLIBS -lpython${VERSION}${ABIFLAGS}"/LIBS="-lpython${VERSION}${ABIFLAGS} @LIBS@ $SYSLIBS"/' \
  ndk-dl/python/Python-2.7.5/Misc/python-config.sh.in
ln -s gdb-7.6 $NDK/sources/android/libthread_db/gdb-7.7
sed -i -e 's/CONFIGURE_FLAGS=$CONFIGURE_FLAGS" --disable-inprocess-agent"/CONFIGURE_FLAGS=$CONFIGURE_FLAGS" \
  --disable-inprocess-agent --enable-werror=no"/' \
  $NDK/build/tools/build-gdbserver.sh
sed -i -e 's/&& version_is_at_least/\&\& ! version_is_at_least/' $NDK/build/tools/build-gnu-libstdc++.sh

## Build NDK - proper build
export NDK_TMPDIR=~/wd/ndk-tmp
mkdir -p $NDK_TMPDIR/release-r10e
time bash $NDK/build/tools/make-release.sh --toolchain-src-dir=$HOME/wd/ndk-dl --release=r10e --incremental


## Repackage
cd /tmp/ndk-android/release/
tar xf android-ndk-r10e-linux-x86.tar.bz2
time 7za a -sfx ~/wd/android-ndk-r10e-linux-x86.bin android-ndk-r10e/ >/dev/null

ls -lh ~/wd/android-ndk-r10e-linux-x86.bin
