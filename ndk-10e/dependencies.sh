#!/bin/bash -ex

# Configuration
echo 'APT::Install-Recommends "false";' > /etc/apt/apt.conf.d/00InstallRecommends

# clean-up non-free sources
sed -i -e 's/ restricted//' -e 's/ multiverse//' /etc/apt/sources.list
dpkg --add-architecture i386
apt-get update

echo 'dash dash/sh boolean false' | debconf-set-selections
DEBIAN_FRONTEND=noninteractive dpkg-reconfigure --pri=high dash



export DEBIAN_FRONTEND=noninteractive

# Base Android build dependencies (from https://source.android.com/source/initializing.html, section "Installing required packages (Ubuntu 12.04)")
# Dropping mingw to skip windows builds for now.
# Installing g++-multilib first otherwise apt-get complains.
apt-get -y install build-essential g++-multilib
apt-get -y install bison git ca-certificates gperf libxml2-utils make python-networkx zlib1g-dev:i386 zip
apt-get -y install libncurses5-dev:i386
apt-get -y install libncurses5-dev  # needed?

# Additional NDK build dependencies (completed from https://android.googlesource.com/platform/ndk/+/master/README.md)
apt-get -y install curl texinfo bison flex libtool pbzip2 groff
apt-get -y install autoconf automake

# Repackaging
apt-get -y install p7zip-full
