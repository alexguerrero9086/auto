#!/bin/bash -ex

# Configuration
echo 'APT::Install-Recommends "false";' > /etc/apt/apt.conf.d/00InstallRecommends

# clean-up non-free sources
cat <<EOF > /etc/apt/sources.list
deb http://old-releases.ubuntu.com/ubuntu/ hardy main universe
deb http://old-releases.ubuntu.com/ubuntu/ hardy-updates main universe
deb http://old-releases.ubuntu.com/ubuntu/ hardy-security main universe
#deb http://old-releases.ubuntu.com/ubuntu/ hardy-backports main universe
EOF
#dpkg --add-architecture i386
apt-get update

echo 'dash dash/sh boolean false' | debconf-set-selections
DEBIAN_FRONTEND=noninteractive dpkg-reconfigure --pri=high dash


# Java
# Your version is: java version "1.6.0_35".
# The correct version is: 1.5.
#apt-get install -y openjdk-6-jdk
apt-get -y install wget
debconf-set-selections <<'EOF'
sun-java5-jdk shared/accepted-sun-dlj-v1-1 boolean true
EOF
# amd64
#wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-jdk_1.5.0-22-0ubuntu0.8.04_amd64.deb
#wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-jre_1.5.0-22-0ubuntu0.8.04_all.deb
#wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-bin_1.5.0-22-0ubuntu0.8.04_amd64.deb
#wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-demo_1.5.0-22-0ubuntu0.8.04_amd64.deb
#checksumfile=$(mktemp)
#cat <<'EOF' > $checksumfile
#a5c1c162cdcf7fe9e3188d0ac081a983179a6defacc85d344c87ae298ea0e28  sun-java5-jdk_1.5.0-22-0ubuntu0.8.04_amd64.deb
#89866c3c359e71a14d90a6e851c9a09b941a768359fa1e3fdb973c7a8f850f5d  sun-java5-jre_1.5.0-22-0ubuntu0.8.04_all.deb
#c63b41410819740ee64d255a7198e666c536dfa14dcf50bd816a3318dacac88e  sun-java5-bin_1.5.0-22-0ubuntu0.8.04_amd64.deb
#5fa084a550371fa8b761144513138cad5f6c60ed53e361674eb02be814f71c9a  sun-java5-demo_1.5.0-22-0ubuntu0.8.04_amd64.deb
#EOF
# i386
wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-jdk_1.5.0-22-0ubuntu0.8.04_i386.deb
wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-jre_1.5.0-22-0ubuntu0.8.04_all.deb
wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-bin_1.5.0-22-0ubuntu0.8.04_i386.deb
wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-demo_1.5.0-22-0ubuntu0.8.04_i386.deb
checksumfile=$(mktemp)
cat <<'EOF' > $checksumfile
daf602b142c1125eb65126c6bfd416e6d56cb17a98e755a881251646abfc3240  sun-java5-jdk_1.5.0-22-0ubuntu0.8.04_i386.deb
89866c3c359e71a14d90a6e851c9a09b941a768359fa1e3fdb973c7a8f850f5d  sun-java5-jre_1.5.0-22-0ubuntu0.8.04_all.deb
632ae901139246b829d2493b94f22a030f35dac604bb27d71f4b4bc3355695a5  sun-java5-bin_1.5.0-22-0ubuntu0.8.04_i386.deb
2e9a98d504831e7f9abee71a77c70e8c1de7b5d335dad540fe30311e3ff0e287  sun-java5-demo_1.5.0-22-0ubuntu0.8.04_i386.deb
EOF
sha256sum -c $checksumfile || exit 1
rm -f $checksumfile
dpkg -i \
  sun-java5-jdk_1.5.0-22-0ubuntu0.8.04_i386.deb \
  sun-java5-jre_1.5.0-22-0ubuntu0.8.04_all.deb \
  sun-java5-bin_1.5.0-22-0ubuntu0.8.04_i386.deb \
  sun-java5-demo_1.5.0-22-0ubuntu0.8.04_i386.deb \
  || true
apt-get -y -f install


# https://source.android.com/setup/build/initializing
apt-get install -y git-core gnupg flex bison gperf build-essential zip \
  curl zlib1g-dev libc6-dev libncurses5-dev \
  x11proto-core-dev libx11-dev libreadline5-dev libz-dev \
  libgl1-mesa-dev g++-multilib mingw32 tofrodos python-markdown \
  libxml2-utils xsltproc unzip
#ln -s /usr/lib32/mesa/libGL.so.1 /usr/lib32/mesa/libGL.so

# Repo
apt-get install -y curl ca-certificates git-core libcurl4-openssl-dev
# Note: neither python2.6 nor python3.1 are supported, need to rebuild Python 2.7
