#!/bin/bash -ex
VERSION='30'

# https://developer.android.com/studio/releases/emulator#emulator_for_arm64_hosts
~/bin/repo init --quiet -u https://android.googlesource.com/platform/manifest -b emu-$VERSION-release --depth=1
~/bin/repo sync --quiet --current-branch -j4
