Rebuild Android components (SDK, NDK...), unattended.

Uses Docker environments and build recipes from
http://android-rebuilds.beuc.net/
