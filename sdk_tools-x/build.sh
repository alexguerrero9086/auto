#!/bin/bash -ex

# Needed?
export USER=$(whoami)

#tools/base/bazel/bazel version

# SDK tools
# https://sites.google.com/a/android.com/tools/build/#TOC-Building-the-Linux-and-MacOS-SDK
mkdir -p out/dist

# Install required android.jar -- move to dl.sh and https://android-rebuilds.beuc.net/dl/prebuilts/sdk/ when available
wget -c https://android-rebuilds.beuc.net/dl/repository/sdk-repo-linux-platforms-userdebug.8.1.0_r61.zip
unzip sdk-repo-linux-platforms-userdebug.8.1.0_r61.zip android-8.1.0/android.jar
mkdir -p prebuilts/studio/sdk/linux/platforms/android-27/
mv android-8.1.0/android.jar prebuilts/studio/sdk/linux/platforms/android-27/android.jar

# build_tools.sh wants to create this directory
rm -rf out/dist/host-test-reports/
# Build
bash -x tools/buildSrc/servers/build_tools.sh `pwd`/out `pwd`/out/dist 0

# Android Studio
# https://android.googlesource.com/platform/tools/base/+/studio-master-dev/studio.md
# fails with studio-3.4.0 ("Compilation failed with exception: Compilation failed")
(
    cd tools/idea/
    bash -x ./build_studio.sh
)

# Gradle plugin
# https://android.googlesource.com/platform/tools/base/+/studio-master-dev/build-system/README.md
# http://tools.android.com/build/gradleplugin
(
    cd tools/
    ./gradlew assemble
    # TODO: this asks for a SDK, plus with more recent buildtools than we just compiled
    ./gradlew publishLocal || true
)
# buildscript {
#    repositories {
#        jcenter()
#        maven { url '.../out/repo' }
#    }
#    dependencies {
#        classpath 'com.android.tools.build:gradle:3.2.0-dev'

# The resulting repo lacks packages:
# com.android.tools.build:bundletool:0.5.0 com.android.tools.build.jetifier:jetifier-core:1.0.0-alpha10 com.android.tools.build.jetifier:jetifier-processor:1.0.0-alpha10
# which are present in the prebuilts


cat tools/base/files/tools_source.properties  # + sdk-repo-linux-tools-0.zip/tools/source.properties
ls -lh out/dist/  # SDK tools
ls -lh tools/idea/out/studio/dist/  # Android Studio
ls -lh out/repo/  # Android Gradle plugin
